package com.epam.engx.story.controller;

import com.epam.engx.story.model.Employee;
import com.epam.engx.story.model.Project;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.regex.Pattern;

@Controller
public class ProjectController {
    @QueryMapping
    @Transactional(timeout = 30)
    public Project projectById(@Argument Integer id) {
        if (Pattern.matches("[0-9]{1}", id.toString())) {
            return Project.getById(id);
        } else {
            throw new RuntimeException("Invalid Input from User");
        }
    }

    @QueryMapping
    @Transactional(timeout = 30)
    public List<Project> projectByEmployee(@Argument String id) throws InterruptedException {
        if (Pattern.matches("^[ A-Za-z0-9-]*", id)) {
            return Project.getByEmployee(id);
        } else {
            throw new RuntimeException("Invalid Input from User");
        }
    }

    @SchemaMapping
    public Employee employee(Project project) {
        return Employee.getById(project.getEmployeeId());
    }
}
