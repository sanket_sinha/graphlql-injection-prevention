package com.epam.engx.story;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphQlInjectionPreventionApplication {

	public static void main(String[] args) {
		SpringApplication.run(GraphQlInjectionPreventionApplication.class, args);
	}

}
