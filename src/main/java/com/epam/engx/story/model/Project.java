package com.epam.engx.story.model;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Project {

    private Integer id;
    private String name;
    private String clientName;
    private String employeeId;

    public Project(Integer id, String name, String clientName, String employeeId) {
        this.id = id;
        this.name = name;
        this.clientName = clientName;
        this.employeeId = employeeId;
    }

    private static List<Project> projects = Arrays.asList(
            new Project(1, "Project 1", "Client 1", "employee-1"),
            new Project(2, "Project 2", "Client 2", "employee-2"),
            new Project(3, "Project 3", "Client 3", "employee-3"),
            new Project(4, "Project 4", "Client 4", "employee-1"),
            new Project(5, "Project 5", "Client 5", "employee-1")
    );

    public static Project getById(Integer id) {
        return projects.stream().filter(project -> project.getId().equals(id)).findFirst().orElse(null);
    }
    public static List<Project> getByEmployee(String id) throws InterruptedException {
        return projects.stream().filter(project -> project.getEmployeeId().equals(id)).collect(Collectors.toList());
    }
    public Integer getId() {
        return id;
    }

    public String getEmployeeId() {
        return employeeId;
    }
}