package com.epam.engx.story.model;

import java.util.Arrays;
import java.util.List;

public class Employee {

    private String id;
    private String firstName;
    private String lastName;

    private List<Project> projects;

    public Employee(String id, String firstName, String lastName, List<Project> projects) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.projects = projects;
    }

    private static List<Employee> employees;

    static {
        try {
            employees = Arrays.asList(
                    new Employee("employee-1", "John", "Doe", Project.getByEmployee("employee-1")),
                    new Employee("employee-2", "Sherlock", "Holmes",Project.getByEmployee("employee-2")),
                    new Employee("employee-3", "Bruce", "Wayne",Project.getByEmployee("employee-3"))
            );
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static Employee getById(String id) {
        return employees.stream().filter(author -> author.getId().equals(id)).findFirst().orElse(null);
    }

    public String getId() {
        return id;
    }
}